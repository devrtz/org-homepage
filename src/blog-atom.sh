#!/bin/sh

# Generate atom feed from org-mode files.

# Written in 2018 by Mohammed Sadiq <sadiq@sadiqpk.org>

# To the extent possible under law, the author(s) have dedicated all
# copyright and related and neighboring rights to this software to the
# public domain worldwide.  This software is distributed without any
# warranty.

# You should have received a copy of the CC0 Public Domain Dedication
# along with this software.  If not, see
# <https://creativecommons.org/publicdomain/zero/1.0/>.

AUTHOR="Mohammed Sadiq"
URL="https://www.sadiqpk.org"
DIRECTORY="blog"
TIMEZONE="+05:30"

files=$(find src/blog/ -iname '*.org' -not -iname 'blog.org')

DATE=$(date -Iseconds)
# Some busybox date doesn't have --rfc-3339.  Also, %z format seems
# not to have ':' in the timezone spec.  And we need RFC 3339 format
# for Atom.  In such cases, ie, if the last three characters are numbers,
# insert a ':' before the second last number.
DATE=$(echo "$DATE" | sed 's/\([0-9]\)\([0-9][0-9]\)$/\1:\2/')

cat << EOF > ${DIRECTORY}.xml
<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">

  <title>${AUTHOR}'s blog</title>
  <link rel="self" href="$URL/blog.xml"/>
  <link href="$URL/"/>
  <updated>$DATE</updated>
  <author>
    <name>$AUTHOR</name>
  </author>
  <id>$URL/$DIRECTORY/</id>

EOF

get_date ()
{
  file="$1"
  type="^#+${2}:"
  DATE=$(grep "$type" -i "$file")

  # If the org file contain #+created/#+date field, use that.
  # Else use the file modification date if updated date requested.
  if [ "$DATE" ]; then
    # Remove org specific formatting characters
    DATE=${DATE#*<}
    DATE=${DATE%>}
    # Remove Day name like "Mon", "Tue", etc.
    DATE=$(echo "$DATE" | sed 's/[A-Z][a-z]*//')
    DATE=$(date -Iseconds -d "${DATE% }")
    echo "${DATE%+*}$TIMEZONE"
  elif [ "$type" = "date" ]; then
    date -Iseconds -r "$file"
  fi
}

get_file_content ()
{
  html_file="$1"

  if [ ! -e "$html_file" ]; then
    return
  fi

  CONTENT=$(awk '/="content/,/="postamble/' "$html_file")
  CONTENT=$(echo "$CONTENT" | sed -n '/="title"/!p')
  CONTENT=$(echo "$CONTENT" | sed -n '/="postamble"/!p')
  echo "$CONTENT"
}

for file in $files
do
  TITLE=$(grep "^#+title:" -i "$file")
  TITLE=${TITLE#*:}
  PUBLISHED=$(get_date "$file" "created")
  UPDATED=$(get_date "$file" "date")
  FILE_URL="${file#src/}"
  FILE_URL="${FILE_URL%.org}.html"
  CONTENT=$(get_file_content "public/$FILE_URL")

  if [ -z "$CONTENT" ]; then
    continue
  fi

cat << EOF >> ${DIRECTORY}.xml
  <entry>
    <title>$TITLE</title>
    <link href="$FILE_URL"/>
    <id>$URL/$FILE_URL</id>
EOF

if [ "$PUBLISHED" ]; then
  echo "    <published>$PUBLISHED</published>" >> ${DIRECTORY}.xml
fi

cat << EOF >> ${DIRECTORY}.xml
    <updated>$UPDATED</updated>
    <content type="html"><![CDATA[$CONTENT]]></content>
  </entry>

EOF

done

# Close the open feed tag
echo "</feed>" >> ${DIRECTORY}.xml

# Publish the generated xml
mv ${DIRECTORY}.xml public/
